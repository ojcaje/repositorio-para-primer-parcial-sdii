import java.io.*;
import java.net.*;
import org.json.*;

class MainCliente
{
	public static void main(String a[]) throws Exception
	{
        // Datos necesario
        String direccionServidor = "127.0.0.1";
		int puertoServidor = 9876;
		
		// Dato para enviar
		JSONObject datoJSON = new JSONObject();
		datoJSON.put("id_estacion", new String("e1"));
		datoJSON.put("ciudad", new String("Villa Elisa"));
		datoJSON.put("porcentaje_humedad", new String("20%"));
		datoJSON.put("temperatura", new String("30"));
		datoJSON.put("velocidad_viento", new String("1km/h"));
		datoJSON.put("fecha", new String("12/09/2020"));
		datoJSON.put("hora", new String("10:28"));
		
        try {
            DatagramSocket clientSocket = new DatagramSocket();

            InetAddress IPAddress = InetAddress.getByName(direccionServidor);
            System.out.println("Intentando conectar a = " + IPAddress + ":" + puertoServidor +  " via UDP...");

            byte[] sendData = new byte[1024];

			String datoPaquete = datoJSON.toString(); 
            sendData = datoPaquete.getBytes();

			System.out.println("Enviar " + datoPaquete + " al servidor. ("+ sendData.length + " bytes)");
			
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, puertoServidor);
            clientSocket.send(sendPacket);
			clientSocket.close();

        } catch (UnknownHostException ex) {
            System.err.println(ex);
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }
} 

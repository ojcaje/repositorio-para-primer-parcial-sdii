import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.Scanner;
import java.util.Vector;

import org.json.*;

public class MainServer
{
	private static Vector<JSONObject> datosRecibidos = new Vector<JSONObject>(0,1);
	public static void main(String[] a){
        
		int puertoServidor = 9876;
		
		// dato para recibir
		JSONObject datoJSON;
        
		try
		{
            DatagramSocket serverSocket = new DatagramSocket(puertoServidor);
			
            byte[] receiveData = new byte[1024];

            // Servidor siempre esperando
			while (true)
			{
				System.out.println("Ingrese el tipo de operacion (1,2,3):");
				Scanner leer = new Scanner(System.in);
				int operacion = leer.nextInt();

				switch (operacion) {
					case 1:
						receiveData = new byte[1024];
						DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
		
						System.out.println("Esperando a algun cliente... ");
		
						// Receive
						serverSocket.receive(receivePacket);
						
						// Datos recibidos
						String datoRecibido = new String(receivePacket.getData());
						datoJSON = new JSONObject(datoRecibido);
						System.out.println("Dato JSON recibido: " + datoJSON.toString() );
						System.out.println("la estacion es: " + datoJSON.get("id_estacion") );
						System.out.println("la ciudad es: " + datoJSON.get("ciudad") );
						System.out.println("el porcentaje_humedad: " + datoJSON.get("porcentaje_humedad") );
						System.out.println("la temperatura: " + datoJSON.get("temperatura") + " grados");
						System.out.println("velocidad viento: " + datoJSON.get("velocidad_viento") );
						System.out.println("fecha: " + datoJSON.get("fecha") );
						System.out.println("hora: " + datoJSON.get("hora") );
						
						// agregar al Vector de datos
						datosRecibidos.add(datoJSON);
						System.out.println("Recepcion de datos exitosa");
						break;
					case 2:
						System.out.println("Ingrese la ciudad:");
						leer = new Scanner(System.in);
						String ciudadRequerida = leer.nextLine();
						consultarTemperatura(ciudadRequerida);
						break;
					case 3:
						System.out.println("Ingrese la fecha (DD/MM/AAAA):");
						leer = new Scanner(System.in);
						String fechaRequerida = leer.nextLine();
						temperaturaPromedio(fechaRequerida);
						break;
					default:
						System.out.println("Por favor verifique que la opcion sea correcta y vuelva a ingresar");
						break;
				}
				
            }
        } catch (Exception ex) {
        	ex.printStackTrace();
            System.exit(1);
        }
	}
	
	public static void consultarTemperatura(String ciudad)
	{
		String ciudadActual;
		for (JSONObject aux: datosRecibidos)
		{
			ciudadActual = aux.get("ciudad").toString();
			if(ciudadActual.compareTo(ciudad)==0)
			{
				System.out.println("La temperatura es de: " + aux.get("temperatura") + " grados");
				return;
			}
		}
		System.out.println("No se encontro la ciudad");
	}

	public static void temperaturaPromedio(String fechaRequerida)
	{
		int promedio=0, total=0;
		String fechaActual;
		boolean seEncontroFecha=false;
		for (JSONObject aux: datosRecibidos)
		{
			fechaActual = aux.get("fecha").toString();
			if(fechaActual.compareTo(fechaRequerida)==0)
			{
				total++;
				seEncontroFecha=true;
				promedio = promedio + Integer.parseInt(aux.get("temperatura").toString());
			}
		}
		if (seEncontroFecha) System.out.println("El promedio es de: " + promedio/total + " grados");
		else System.out.println("No se encontro algun dato de la fecha especificada");
	}
}